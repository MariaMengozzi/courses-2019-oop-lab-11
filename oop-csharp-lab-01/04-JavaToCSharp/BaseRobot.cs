﻿using _04_JavaToCSharp;
using System;

namespace JavaToCSharp
{
    public class BaseRobot : IRobot
    {
        public const double BATTERY_FULL = 100;
        public const double MOVEMENT_DELTA_CONSUMPTION = 1.2;
        private const int MOVEMENT_DELTA = 1;

        private double batteryLevel;
        private readonly RobotEnvironment environment;
        private readonly string robotName;

        public BaseRobot(string robotName)
        {
            this.environment = new RobotEnvironment(new RobotPosition(0, 0));
            this.robotName = robotName;
            this.batteryLevel = BATTERY_FULL;

        }

        protected void ConsumeBattery(double amount)
        {
            if (batteryLevel >= amount)
            {
                this.batteryLevel -= amount;
            }
            else
            {
                this.batteryLevel = 0;
            }
        }

        private void ConsumeBatteryForMovement()
        {
            ConsumeBattery(GetBatteryRequirementForMovement());
        }

        protected double GetBatteryRequirementForMovement()
        {
            return MOVEMENT_DELTA_CONSUMPTION;
        }

        protected bool IsBatteryEnough(double operationCost)
        {
            return batteryLevel > operationCost;
        }

        protected void Log(string msg)
        {
            Console.WriteLine("[" + this.robotName + "]: " + msg);
        }

        private bool Move( int dx, int dy)
        {
            if (IsBatteryEnough(GetBatteryRequirementForMovement()))
            {
                if (environment.Move(dx, dy))
                {
                    ConsumeBatteryForMovement();
                    Log("Moved to position " + environment.Position + ". Battery: " + BatteryLevel + "%.");
                    return true;
                }
               Log("Can not move of (" + dx + "," + dy
                        + ") the robot is touching the world boundary: current position is " + environment.Position);
            }
            else
            {
                Log("Can not move, not enough battery. Required: " + GetBatteryRequirementForMovement()
                    + ", available: " + batteryLevel + " (" + BatteryLevel + "%)");
            }
            return false;
        }

        public bool MoveUp()
        {
            return Move(0, MOVEMENT_DELTA);
        }

        public bool MoveDown()
        {
            return Move(0, -MOVEMENT_DELTA);
        }

        public bool MoveLeft()
        {
            return Move(-MOVEMENT_DELTA, 0); ;
        }

        public bool MoveRight()
        {
            return Move(MOVEMENT_DELTA, 0);
        }

        public void Recharge()
        {
            this.batteryLevel = BATTERY_FULL;
        }

        public double BatteryLevel
        {
            get { return Math.Round(batteryLevel * 100d) / BATTERY_FULL; }
            private set {; }
        }

        public RobotPosition Position
        {
            get { return environment.Position; }
        }
    }
}