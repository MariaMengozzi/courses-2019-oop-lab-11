﻿using _04_JavaToCSharp;

namespace JavaToCSharp
{
    public class RobotEnvironment
    {
        /**
     * Environment upper bound for the X coordinate
     */
        public const int X_UPPER_LIMIT = 50;
        /**
         * Environment lower bound for the X coordinate
         */
        public const int X_LOWER_LIMIT = 0;
        /**
         * Environment upper bound for the Y coordinate
         */
        public const int Y_UPPER_LIMIT = 80;
        /**
         * Environment lower bound for the X coordinate
         */
        public const int Y_LOWER_LIMIT = 0;

        private RobotPosition position;

        public RobotEnvironment(RobotPosition position)
        {
            Position = position;
        }

        protected bool IsWithinWorld(RobotPosition p)
        {
            var x = p.X;
            var y = p.Y;
            return x >= X_LOWER_LIMIT && x <= X_UPPER_LIMIT && y >= Y_LOWER_LIMIT && y <= Y_UPPER_LIMIT;
        }

        public bool Move(int dx, int dy)
        {
            var newPos = Position + new RobotPosition(dx, dy);
            if (IsWithinWorld(newPos))
            {
                Position = newPos;
                position = newPos;
                return true;
            }
            return false;
        }

        public RobotPosition Position
        {
            get;private set;
        }
    }
}