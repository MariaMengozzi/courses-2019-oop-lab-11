﻿namespace _04_JavaToCSharp
{
    public class RobotPosition ///: IPosition2D
    {

        public RobotPosition(int x, int y)
        {
            X = x;
            Y = y;
        }

        public override bool Equals(object o)
        {
            if (o is RobotPosition)
            {
                RobotPosition p = (RobotPosition)o;
                return X == p.X && Y == p.Y;
            }
            return false;
        }

        public override int GetHashCode()
        {
            /*
             * This could be implemented WAY better.
             */
            return X ^ Y;
        }

        public int X
        {
            get;private set;
        }

        public int Y
        {
            get; private set;
        }

        public static RobotPosition operator+ (RobotPosition p1, RobotPosition p2)
        {
            return new RobotPosition(p1.X + p2.X, p1.Y + p2.Y); ;
        }

        public override string ToString()
        {
            return "[" + X + ", " + Y + "]";
        }

    }
}