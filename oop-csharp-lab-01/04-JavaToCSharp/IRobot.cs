﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp
{
    interface IRobot
    {
        /**
     * Moves the robot up by one unit
     * 
     * <returns>true if a movement has been performed </returns>
     */
        bool MoveUp();

        /**
         * Moves the robot down by one unit
         * 
         * <return>true if a movement has been performed</return> 
         */
        bool MoveDown();

        /**
         * Moves the robot left by one unit
         * 
         * <returns>true if a movement has been performed </returns>
         */
        bool MoveLeft();

        /**
         * Moves the robot right by one unit
         * 
         * <returns>true if a movement has been performed </returns>
         */
        bool MoveRight();

        /**
         * Fully recharge the robot
         */
        void Recharge();

    }
}
