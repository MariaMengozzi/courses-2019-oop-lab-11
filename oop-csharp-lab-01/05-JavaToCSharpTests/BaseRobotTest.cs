﻿using System;
using _04_JavaToCSharp;
using JavaToCSharp;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace _05_JavaToCSharpTests
{
    [TestClass]
    public class BaseRobotTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            BaseRobot br1 = new BaseRobot("br1");
            for (int x = 0; x <= RobotEnvironment.Y_UPPER_LIMIT; x++)
            {
                Assert.AreEqual(new RobotPosition(0, x), br1.Position);
                br1.MoveUp();
            }
            br1.MoveUp();
            Assert.AreEqual(new RobotPosition(0, RobotEnvironment.Y_UPPER_LIMIT), br1.Position);
            Assert.AreEqual(4, br1.BatteryLevel);

            for (int x = 0; x <= 3; x++)
            {
                Assert.AreEqual(new RobotPosition(x,RobotEnvironment.Y_UPPER_LIMIT), br1.Position);
                br1.MoveRight();
            }
            Assert.AreEqual(0.4, br1.BatteryLevel);
            br1.MoveRight();
            for (int x = 0; x < 20; x++)
            {
                Assert.AreEqual(new RobotPosition(3, RobotEnvironment.Y_UPPER_LIMIT), br1.Position);
                Assert.AreEqual(0.4, br1.BatteryLevel);
            }
        }
    }
}