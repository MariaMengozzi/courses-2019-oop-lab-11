﻿using System;

namespace ComplexNumbers
{
    class ComplexNum
    {
        public ComplexNum(double re, double im)
        {
            Re = re;
            Im = im;
        }

        public double Re { get; private set; }
        public double Im { get; private set; }

        //overraid operator +
        public static ComplexNum operator+ (ComplexNum n1, ComplexNum n2)
        {
            return new ComplexNum(n1.Re + n2.Re , n1.Im + n2.Im);
        }

        //overraid operator -
        public static ComplexNum operator -(ComplexNum n1, ComplexNum n2)
        {
            return new ComplexNum(n1.Re - n2.Re, n1.Im - n2.Im);
        }

        //overraid operator *
        public static ComplexNum operator *(ComplexNum n1, ComplexNum n2)
        {
            return new ComplexNum((n1.Re * n2.Re) - (n1.Im * n2.Im) , (n1.Re * n2.Im) + (n1.Im * n2.Re));
        }

        //overraid operator /
        public static ComplexNum operator /(ComplexNum n1, ComplexNum n2)
        {
            return new ComplexNum(((n1.Re*n2.Re) + (n1.Im * n2.Im)) / (Math.Pow(n2.Re,2) + Math.Pow(n2.Im, 2)) , 
                ((n1.Im * n2.Re) - (n1.Re * n2.Im)) / (Math.Pow(n2.Re, 2) + Math.Pow(n2.Im, 2)));
        }

        // Restituisce il modulo del numero complesso
        public double Module
        {
            get
            {
                return Math.Sqrt(Math.Pow(Re, 2) + Math.Pow(Im, 2));
            }
        }

        internal void Invert()
        {
            ComplexNum inverso = new ComplexNum(1, 0) / this;
            Re = inverso.Re;
            Im = inverso.Im;
        }

        // Restituisce il complesso coniugato del numero complesso (https://en.wikipedia.org/wiki/Complex_conjugate)
        public ComplexNum Conjugate
        {
            get
            {
                return new ComplexNum(Re, Im * -1);
            }
        }

        // Restituisce una rappresentazione idonea per il numero complesso
        public override string ToString()
        {
            return "numero: " + Re + (Im > 0 ? "+" : "" ) + Im +"i";
        }
    }
}
