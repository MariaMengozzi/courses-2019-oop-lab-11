﻿using System;

namespace Cards
{
    class Deck
    {
        private Card[] cards;

        public Deck()
        {
            cards = new Card[Enum.GetNames(typeof(ItalianSeed)).Length * Enum.GetNames(typeof(ItalianValue)).Length];
        }

        public void Initialize()
        {
            /*
             * == README ==
             * 
             * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano rispettivamente semi e valori
             * di un tipo mazzo di carte da gioco italiano.
             * 
             * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni possibili.
             * 
             * Nota - Dato un generico enumerato MyEnum:
             *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
             *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
             * 
             */
            int i = 0;
            foreach (string seed in Enum.GetNames(typeof(ItalianSeed)))
            {
                foreach (string itValue in Enum.GetNames(typeof(ItalianValue)))
                {
                    cards[i] = new Card(itValue, seed);
                    i++;
                }
            }
        }

        public void Print()
        {
            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */
            foreach (Card card in cards)
            {
                Console.WriteLine(card.ToString());
            }
        }

        public Card CardIndex(ItalianSeed seme, ItalianValue valore)
        {
            foreach (Card card in cards)
            {
                if(card.Seed.CompareTo(seme.ToString()) == 0 && card.Value.CompareTo(valore.ToString())== 0)
                {
                    return card;
                }
            }
            throw new FormatException();
        }
    }


    enum ItalianSeed
    {
        DENARI,
        COPPE,
        SPADE,
        BASTONI
    }

    enum ItalianValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        FANTE,
        CAVALLO,
        RE
    }
}
