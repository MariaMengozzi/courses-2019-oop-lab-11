﻿using System;

namespace Cards
{
    class FrenchDeck
    {
        private Card[] cards;
        public const int Jolly = 2;
        public FrenchDeck()
        {
            cards = new Card[(Enum.GetNames(typeof(FrenchSeed)).Length - 1)  * (Enum.GetNames(typeof(FrenchValue)).Length - 1) + Jolly];
        }

        public void Initialize()
        {
            int i = 0;
            foreach (string seed in Enum.GetNames(typeof(FrenchSeed)))
            {
                if (!seed.Equals(FrenchSeed.JOLLY.ToString()))
                {
                   foreach (string frValue in Enum.GetNames(typeof(FrenchValue)))
                    {
                        if (!frValue.Equals(FrenchValue.J.ToString()))
                        {
                            cards[i] = new Card(frValue, seed);
                            i++;
                        }
                    }
                }
            }
            for (int x = 0; x < Jolly; x++)
            {
                cards[i + x] = new Card(FrenchValue.J.ToString(), FrenchSeed.JOLLY.ToString());
            }
        }

        public void Print()
        {
            foreach (Card card in cards)
            {
                Console.WriteLine(card.ToString());
            }
        }

        public Card CardIndex(FrenchSeed seme, FrenchValue valore)
        {
            foreach (Card card in cards)
            {
                if (card.Seed.CompareTo(seme.ToString()) == 0 && card.Value.CompareTo(valore.ToString()) == 0 )
                {
                    return card;
                }
            }
            throw new FormatException();
        }
    }


    enum FrenchSeed
    {
        PICCHE,
        QUADRI,
        FIORI,
        CUORI,
        JOLLY
    }

    enum FrenchValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        FANTE,
        CAVALLO,
        RE,
        J
    }
}
